package br.senai.sp.informatica.oop;

public class ContaTeste {

	public static void main(String[] args) {
		Conta conta = new Conta();
		conta.clientes= "Marcelo Henrique";
		conta.clientes = "Marcelo";
		conta.saldo = 10_000_00;
		
		Conta destino = new Conta ();
		destino.clientes = "Bob";
		destino.saldo = 10.00;
		
		//TRANSFERE DINHEIRO DE MARCELO PARA BOB
		conta.transferePara(destino, 1550);
		
		conta.exibeSaldo();
		destino.exibeSaldo();

	}

}
