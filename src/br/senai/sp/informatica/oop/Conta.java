package br.senai.sp.informatica.oop;

public class Conta {
	
	//ATRIBUTOS
	String clientes;
	double saldo;

	//METODOS
	void exibeSaldo() {
		System.out.println("Cliente " + " seu saldo � de R$ " + saldo );
	}
	void saca(double valor) {
		//SUBTRAI O VALOR DO SALDO
	//	saldo = saldo - valor;
		saldo -= valor;
	}
	void deposita (double valor) {
		saldo += valor;
		
	}
	void transferePara (Conta destino, double valor) {
		this.saca (valor);
		destino.deposita (valor); 
	}
}
